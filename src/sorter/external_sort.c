/*
 * count_sort.c
 * 
 * Copyright 2020 Unknown <sferd@agesa>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */


#include <stdio.h>

#include "debug.h"
#include "lines.h"
#include "sort.h"

int external_sort( const char * source_path, const char * target_path, size_t elem_size, sort_func countsort, value_cmp cmp)
{	
	DBG_OUT;
	
	/* Выполняем дробление исходного файла на кусочки */
	FILE * source = fopen( source_path, "rb");
	line_massive_t * files_lines = readLinesToFiles( source, elem_size);
	fclose( source);
	
	/* Внутренняя сортировка */
	for ( int i = 0; i < files_lines->count; ++i)
	{
		linesort( files_lines->lines + i, files_lines->elem_size, countsort, cmp);
	}
	
	/* Внешняя сортировка (слияние) */
	FILE * target = fopen( target_path, "wb");
	merge_sort( files_lines, target, cmp);
	fclose( target);
	
	return 0;
}
