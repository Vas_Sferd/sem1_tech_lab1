/*
 * vector.c
 * 
 * Copyright 2020 Unknown <sferd@agesa>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */


#include <stdio.h>
#include <malloc.h>
#include <math.h>

#include "debug.h"
#include "vector.h"

vector_t * mkVector( int x, int y, int z)
{	
	vector_t * vector = malloc( sizeof( vector_t));
	
	vector->x = x;
	vector->y = y;
	vector->z = z;
	
	return vector;
}

double vectorLenght( const vector_t * vector)
{
	return sqrt( pow( vector->x, 2) + pow( vector->y, 2) + pow( vector->z, 2));
}

void printVector( void * data)
{
	vector_t * vector = data;
	printf( "x = %d, y = %d, z = %d, len = %.2lf", vector->x, vector->y, vector->z, vectorLenght( vector));
}

int vector_cmp( void * first, void * second)
{
	return ( vectorLenght( first) > vectorLenght( second)) - ( vectorLenght( second) > vectorLenght( first));
}
