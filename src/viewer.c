/*
 * viewer.c
 * 
 * Copyright 2020 Unknown <sferd@agesa>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */


#include <stdio.h>
#include <malloc.h>

#include "debug.h"
#include "types.h"


int main( int argc, char * argv[])
{		
	if ( argc == 2)
	{
		DBG_OUT;
		
		FILE * f = fopen( argv[1], "rb");
		
		vector_t vector;		// Хранилище
		int i = 1;				// Индекс
		
		fread( &vector, sizeof( vector_t), 1, f);
		
		while ( ! feof( f))
		{
			double len = vectorLenght( &vector);
			printf( "%d) x = %d, y = %d, z = %d, len = %.2lf\n", i, vector.x, vector.y, vector.z, len);
			
			++i;
			fread( &vector, sizeof( vector_t), 1, f);
		}
	}
	else
	{
		perror( "Please write filename\n");
	}
	
	return 0;
}

