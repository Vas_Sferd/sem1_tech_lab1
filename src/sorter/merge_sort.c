/*
 * merge_sort.c
 * 
 * Copyright 2020 Unknown <sferd@agesa>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */

#include <stdio.h>
#include <malloc.h>
#include <string.h>

#include "debug.h"
#include "lines.h"
#include "sort.h"

void flback( int * remaining, size_t elem_size, line_t * line)
{
	FILE * f = line->file;
	fpos_t * position = line->position;
	
	fsetpos( f, position);
	fseek( f, -elem_size, SEEK_CUR);
	fgetpos( f, line->position);
	++( * remaining);
	
	fseek( f, 0, SEEK_SET);
}

void * flreadnext( void * value, int * remaining, size_t elem_size, line_t * line)
{
	FILE * f = line->file;
	fpos_t * position = line->position;
	
	fsetpos( f, position);
	fread( value, elem_size, 1, f);
	fgetpos( f, line->position);
	--( * remaining);
	
	fseek( f, 0, SEEK_SET);
}

int merge_sort( line_massive_t * files_lines, FILE * target, value_cmp cmp)
{
	DBG_OUT;
	
	fseek( target, 0, SEEK_SET);
	
	size_t elem_size = files_lines->elem_size;								// Размер элементов
	
	void * min = malloc( elem_size);										// Максимум
	void * current = malloc( elem_size);									// Текущий элемент
	int summa = 0;															// Сумма всех доступных элементов
	int * remaining = malloc( files_lines->count * sizeof( int));			// Массив указывающий на число несчитанных данных в каждой линии
	
	for ( int i = 0; i < files_lines->count; ++i)
	{
		remaining[i] = files_lines->lines[i].count;							// Инициализация
		summa += remaining[i];
	}
	
	for ( ; summa > 0; --summa)
	{
		int i;
		int mini;
		
		/* Поиск индекса первой не пустой линии */
		for ( i = 0; i < files_lines->count; ++i)
		{
			if ( remaining[i] != 0)
				break;
		}
		
		DBG_OUT;
																		
		/* Считываем первый несчитанный элемент */
		flreadnext( min, &remaining[i], elem_size, &files_lines->lines[i]);
		mini = i;
		
		++i;
		
		while ( i < files_lines->count)										// Поиск максимума
		{
			if ( remaining[i] != 0)											// Проверка на наличие несчитанных элементов
			{
				/* Считываем текущее значение */
				flreadnext( current, &remaining[i], elem_size, &files_lines->lines[i]);
				
				if ( cmp( current, min) < 0)
				{
					/* Возвращаем каретку назад для предыдущего максимума */
					flback( &remaining[mini], elem_size, &files_lines->lines[mini]);
					
					/* Обновляем максимум */
					memcpy( min, current, elem_size);
					mini = i;
				}
				else
				{
					/* Возвращаем каретку назад */
					flback( &remaining[i], elem_size, &files_lines->lines[i]);
				}
			}	
			
			++i;	
		}
		
		DBG_OUT;
		fwrite( min, files_lines->elem_size, 1, target);
	}
	
	//DEBUG=============================================================
	#ifdef __DEBUG__
	for ( int i = 0; i < files_lines->count; ++i)
	{
		printf( "[Info] Remaining[%d] = %d\n", i, remaining[i]);
	}
	#endif
	//DEBUG=============================================================
	
	/* Очищаем память */
	free( min);
	free( current);
	
	DBG_OUT;
	
	free( files_lines->lines);
	free( files_lines);
	
	fseek( target, 0, SEEK_SET);	// Помешаем каретку в начало 
	
	return 0;
}
