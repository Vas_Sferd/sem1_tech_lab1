/*
 * debug.h
 * 
 * Copyright 2020 Unknown <sferd@agesa>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */

#ifndef _DEBUG_H
#define _DEBUG_H

//#define __DEBUG__

#ifdef	__DEBUG__
	#include <stdio.h>
	#include <stdlib.h>
	
	#define DBG_OUT printf( "[DEBUG] (%s:%d) It`s ok!\n", __FILE__, __LINE__)  
#else
	#define DBG_OUT 

#endif

#endif
