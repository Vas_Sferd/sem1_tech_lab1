#This is CMake file

cmake_minimum_required(VERSION 2.8)

project( vecsor)
set(LIBRARY_OUTPUT_PATH ${PROJECT_SOURCE_DIR}/bin)

include_directories("src/include/"
					"src/sorter/"
					"src/types/lines/"
					"src/types/vector/")

set(SOURCE_EXE src/main.c)
set(SOURCE_VIEW src/viewer.c)
set(SOURCE_GENFILE src/file_generator.c)
set(SOURCE_LIB	src/sorter/external_sort.c
				src/sorter/count_sort.c
				src/sorter/merge_sort.c
				src/sorter/sort.c
				src/types/lines/lines.c
				src/types/vector/vector.c)


set(HEADER	${PROJECT_SOURCE_DIR}/src/include/sort.h
			${PROJECT_SOURCE_DIR}/src/include/types.h
			${PROJECT_SOURCE_DIR}/src/include/debug.h
			${PROJECT_SOURCE_DIR}/src/types/lines/lines.h
			${PROJECT_SOURCE_DIR}/src/types/vector/vector.h)

add_executable(vecsor ${SOURCE_EXE})
add_executable(view ${SOURCE_VIEW})
add_executable(genfile ${SOURCE_GENFILE})

add_library(libs ${SOURCE_LIB})
target_link_libraries(libs m)

target_link_libraries(vecsor libs m)
target_link_libraries(view libs)
target_link_libraries(genfile libs)
