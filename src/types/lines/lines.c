/*
 * lines.c
 * 
 * Copyright 2020 Unknown <sferd@agesa>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */

#include <malloc.h>
#include <math.h>

#include "debug.h"
#include "lines.h"

line_massive_t * mkLineMass( int count, size_t elem_size) 		// Создание массива линий
{
	DBG_OUT;
	
	line_massive_t * line_mass = malloc( sizeof( line_massive_t));
	
	line_mass->count = count;
	line_mass->elem_size = elem_size;
	line_mass->lines = malloc( count * sizeof( line_t));
	
	return line_mass;
}

line_massive_t * readLinesToFiles( FILE * source, size_t elem_size)	// Дробь файла на фрагменты
{	
	DBG_OUT;
	
	fseek( source, 0, SEEK_SET);
	int start = ftell( source);
	
	fseek( source, 0, SEEK_END);
	int end = ftell( source);
	
	int file_size = end - start;								// Размер файла
	int all_elem_count = file_size / elem_size;					// Общее число объектов
	int line_count = floor( sqrt( all_elem_count)); 		// Число элементов в линии
	int line_elem_count = line_count + 1;						// Число линий
    
    if ( all_elem_count > line_count * line_elem_count)
    {
		++line_count;
	}
    
    DBG_OUT;
    
    //DEBUG=============================================================
    #ifdef __DEBUG__ 
    printf( "[Info] all_elem_count = %d\n", all_elem_count);
    printf( "[Info] line_elem_count = %d\n", line_elem_count);
    printf( "[Info] line_count = %d\n", line_count);
	printf( "[Info] elem_size = %d\n", elem_size);
	#endif
    //DEBUG=============================================================
    
	line_massive_t * files_lines = mkLineMass( line_count, elem_size);
	
	int remaining = all_elem_count;								// Число оставшихся элементов
	void * data = malloc( line_elem_count * elem_size);			// Буфер памяти размеров в один элемент
	
	fseek( source, 0, SEEK_SET);
	FILE * f = tmpfile();										// Файл для хранения данных
	
	for ( int i = 0; i < line_count; ++i)
	{		
		DBG_OUT;
		
		if ( remaining < line_elem_count)
		{
			line_elem_count = remaining;
		}
		
		/* Инициализируем линию */
		files_lines->lines[i].count = line_elem_count;			// Число элеиентов в линии
		files_lines->lines[i].file = f;							// Указываем файл
		files_lines->lines[i].position = malloc( sizeof( fpos_t));
		fgetpos( f, files_lines->lines[i].position);			// Копируем данные во временный файл файл
		
		DBG_OUT;
		
		fread( data, elem_size, line_elem_count, source);		// Считываем данные в буфер
		
		fwrite( data, elem_size, line_elem_count, files_lines->lines[i].file);
		
		remaining -= line_elem_count;
		
		//DEBUG=========================================================
		#ifdef __DEBUG__
		printf("[INFO] remaining = %ld\n", remaining);
		#endif
		//DEBUG=========================================================
	}
	
	//DEBUG=============================================================
	#ifdef __DEBUG__
	printf("[INFO] final remaining = %ld\n", remaining);
	#endif
	//DEBUG=============================================================
	
	if ( remaining != 0)
	{
		perror( "Error splitting file\n");
	}
	
	free( data);
	fseek( source, 0, SEEK_SET);
	
	return files_lines;
}
