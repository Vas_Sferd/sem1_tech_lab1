/*
 * lines.c
 * 
 * Copyright 2020 Unknown <sferd@agesa>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */

#ifndef _LINES_H
#define _LINES_H

#include <stdint.h>

typedef struct
{
	int64_t count;			// Число элементов
	FILE * file;			// Файл
	fpos_t * position;		// Позиция
	
} line_t;

typedef struct
{
	int64_t count;		// Число линий
	size_t elem_size;	// Размер базового элемента линии
	line_t * lines;		// Ссылка на массв контейнеров

} line_massive_t;

//----------------------------------------------------------------------

line_massive_t * mkLineMass( int count, size_t elem_size); 				// Создание массива линий
line_massive_t * readLinesToFiles( FILE * source, size_t elem_size);	// Дробь файла на фрагменты

#endif

