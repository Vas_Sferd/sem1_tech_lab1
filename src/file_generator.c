/*
 * file_generator.c
 * 
 * Copyright 2020 Unknown <sferd@agesa>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */


#include <stdio.h>
#include <stdlib.h>
#include <limits.h>
#include <math.h>

#include "debug.h"
#include "types.h"
#include "sort.h"

/*
 * Здесь выполняется генерация бинарного файла
 *
 * */

int randint( void)
{
	return rand() % 201;
}

int main( int argc, char * argv[])
{
	if ( argc == 3)
	{
		FILE * f = fopen( argv[1], "wb");
		
		srand( 0);
		
		vector_t vector;
		int count = atoi( argv[2]);
		
		for( int i = 0; i < count; ++i)
		{
			vector.x = randint();
			vector.y = randint();
			vector.z = randint();
			
			fwrite( &vector, sizeof( vector_t), 1, f);
		}
	}
	else
	{
		perror( "Incorrect parametrs. Just use genfile <path> <count>");
	}
	
	return 0;
}

