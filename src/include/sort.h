/*
 * types.h
 * 
 * Copyright 2020 Unknown <sferd@agesa>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */

#ifndef _SORT_H
#define _SORT_H

typedef int ( * value_cmp)( void *, void *);					// Функция сравнения 
typedef int ( * sort_func)( void *, size_t, int, value_cmp);	// Функция сортировки

int linesort( line_t * line, size_t elem_size, sort_func data_sort, value_cmp elem_cmp);
int countsort( void * data, size_t size, int count, value_cmp cmp);
int external_sort( const char * source_path, const char * target_path, size_t size, sort_func countsort, value_cmp cmp);
int merge_sort( line_massive_t * file_lines, FILE * target, value_cmp cmp);

#endif
