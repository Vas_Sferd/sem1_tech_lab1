# This is makefile

all:
	if ! [ -d ./bin ]				# Гарантируем существование директории bin
	mkdir bin						#
	fi								#
	
	
	cmake cmake -S ./ -B ./bin		# Сборка
	cd ./bin && make &&				# 
	
	
	if ! [ -d ./exe ]				# Гарантируем существование директории exe
	mkdir exe						#
	fi								#
	
	
	mv ./vecsor ./../exe &&			# Перемещение готовых файлов в ./exe
	mv ./genfile ./../exe &&
	mv ./view ./../exe;
	
	
	cd ..
