/*
 * sort.c
 * 
 * Copyright 2020 Unknown <sferd@agesa>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */


#include <stdio.h>
#include <malloc.h>

#include "debug.h"
#include "lines.h"
#include "sort.h"

int linesort( line_t * line, size_t elem_size, sort_func data_sort, value_cmp elem_cmp)
{	
	DBG_OUT;

	fsetpos( line->file, line->position);								// Смещаемся на начало участка
	
	void * data = malloc( line->count * elem_size);
	fread( data, elem_size, line->count, line->file);

	/* Сортировка */
	int status = data_sort( data, elem_size, line->count, elem_cmp);

	/* Запись */
	fsetpos( line->file, line->position);								// Смещаемся на начало участка
	fwrite( data, elem_size, line->count, line->file);
	
	/* Очистка памяти буфера */
	free( data);
	
	return status;
}
