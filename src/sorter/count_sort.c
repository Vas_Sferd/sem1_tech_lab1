/*
 * count_sort.c
 * 
 * Copyright 2020 Unknown <sferd@agesa>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */

#include <stdio.h>
#include <string.h>
#include <malloc.h>

#include "debug.h"
#include "types.h"
#include "sort.h"

#ifdef __DEBUG__
#define PRINT_ELEM printVector

static void printElems( void * data, size_t size, int count, void ( * printElem)( void *))
{
	void * elem = data;
	
	for ( int i = 0; i < count; ++i)
	{
		printf( "%d) ", i);
		printElem( elem);
		putchar( '\n');
		
		elem += size;
	}
}
#endif

int countsort( void * data, size_t size, int count, value_cmp cmp)
{		
	//DEBUG=============================================================
	#ifdef __DEBUG__
	printf( "Unsorted :\n");
	printElems( data, size, count, PRINT_ELEM);
	#endif
	//DEBUG=============================================================
	
	void * sorted_data = malloc( count * size);				// Копируем участок памяти исходных данных
	
	int positions[count];									// Позиции элементов
	
	void * sample;											// Элемент, для которого находится позиция 
	void * current;											// Текущий элемент 
	
	for ( int i = 0; i < count; ++i)
	{	
		positions[i] = 0;
		sample = data + i * size;
		
		for ( int j = 0; j < count; ++j)
		{
			current = data + j * size;
			
			positions[i] += ( cmp( sample, current) > 0)				// Находим позицию образца
					? 1
					:	( ! cmp( sample, current) && i > j)			// Проверка на равные значения
						? 1
						: 0;
		}

		memcpy( sorted_data + positions[i] * size, sample, size);
	}
	
	/* Записываем изменения */
	memcpy( data, sorted_data, count * size);
	free( sorted_data);
	
	//DEBUG=============================================================
	#ifdef __DEBUG__
	printf( "Sorted :\n");
	printElems( data, size, count, PRINT_ELEM);
	#endif
	//DEBUG=============================================================

	return 0;
}
