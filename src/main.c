/*
 * main.c
 * 
 * Copyright 2020 Unknown <sferd@agesa>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */

#include <stdio.h>
#include <math.h>

#include "debug.h"
#include "types.h"
#include "sort.h"


int main( int argc, char * argv[])
{
	DBG_OUT;
	
	if ( argc == 2 || argc == 3)
	{
		/* Получаем имя исходногоо и выходного файла */ 
		char * source_path = argv[1];
		char * target_path = ( argc == 3)
			? argv[2]
			: source_path;
		
		/* Производим сортировку */
		external_sort( source_path, target_path, sizeof( vector_t), countsort, vector_cmp);
		
		printf( "The program completed successfully. Data has been sorted.\n");
	}
	else
	{
		perror(	"Error: too few or many arguments have been entered\n"
				"Please use only two arguments: <source file> <target file>\n");
	}
	
	return 0;
}
